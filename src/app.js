import Vue from 'vue';
import App from './components/App.vue';

import vueShortkey from 'vue-shortkey';

Vue.use(vueShortkey);

new Vue({
  el: '#app',
  components: {
    App
  },
  render(h) {
    return h('app');
  }
});
