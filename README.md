# RelaxedReader

A Firefox extension to fight reading anxiety.

![screenshot](img/screenshot.png)

## Features

`RelaxedReader` is your typical reading list, with a few important differences:

- You can store a maximum of 10 articles. If you add more, the oldest will get pushed out (FIFO).
- Items automatically expire after 30 days.
- No notifications, badges, unread counts or anything else that prompts anxiety and/or FOMO.
- No tracking, third party servers or API calls. All data is stored in [extension storage](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/storage).
- Uses `storage.sync`, so the reading list is synced across all instances of the browser that the user is logged into, across different devices.

## Keyboard shortcuts

| Shortcut                   | Action                                                |
| -------------------------- | ----------------------------------------------------- |
| Ctrl-Shift-L / Cmd-Shift-L | Open `RelaxedReader` pop-up                           |
| Enter                      | Add current tab to reading list (when pop-up is open) |
| c                          | Clear reading list (when pop-up is open)              |

## Building

To use an unreleased version follow these steps:

1. Check out this repository.
2. Run `yarn build`.
3. Zip the contents of the `dist/` directory (do NOT include the directory itself).
4. Go to <a href="about:debugging">about:debugging</a>.
5. Click "Enable add-on debugging".
6. Click "Load Temporary Add-on…" and select the zip file generated in step 3.

## Thanks

* [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/)
* [Vue.js](https://vuejs.org/)
* [vue-shortkey](https://github.com/iFgR/vue-shortkey)
* [Day.js](https://github.com/iamkun/dayjs)
* [Visual Studio Code](https://code.visualstudio.com/)

## License

Licensed under [the MIT license](./LICENSE).
